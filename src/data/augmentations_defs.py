import cv2
from matplotlib import pyplot as plt
import albumentations as A
import numpy as np
import cv2
import os, random


# Добавление водяных знаков
def add_wm(background, p=0.33):
    rand = random.randint(0, 20)
    x = -1
    for x_offset in range(rand, 524, 80):
        x += 1
        y = -1
        for y_offset in range(368 + rand, 733, 73):
            y += 1
            if random.uniform(0, 1) < p:
                # случайный выбор знака
                wm_eagle_file = random.choice(
                    os.listdir("images/watermarks/watermarks/eagle/")
                )
                wm_star_file = random.choice(
                    os.listdir("images/watermarks/watermarks/star/")
                )
                wm_eagle = cv2.imread(
                    f"images/watermarks/watermarks/eagle/{wm_eagle_file}",
                    cv2.IMREAD_UNCHANGED,
                )
                wm_star = cv2.imread(
                    f"images/watermarks/watermarks/star/{wm_star_file}",
                    cv2.IMREAD_UNCHANGED,
                )
                # обрезка
                k = random.randint(1, 3)
                wm_eagle = wm_eagle[0 : int(70 / k), :]
                wm_star = wm_star[0 : int(70 / k), :]
                # поочередная расстановка по сетке
                if x % 2 == 0:
                    if y % 2 != 0:
                        foreground = wm_eagle
                    else:
                        foreground = wm_star
                else:
                    if y % 2 == 0:
                        foreground = wm_eagle
                    else:
                        foreground = wm_star

                bg_h, bg_w, bg_channels = background.shape
                fg_h, fg_w, fg_channels = foreground.shape

                assert (
                    bg_channels == 3
                ), f"background image should have exactly 3 channels (RGB). found:{bg_channels}"
                assert (
                    fg_channels == 4
                ), f"foreground image should have exactly 4 channels (RGBA). found:{fg_channels}"

                # center by default
                if x_offset is None:
                    x_offset = (bg_w - fg_w) // 2
                if y_offset is None:
                    y_offset = (bg_h - fg_h) // 2

                w = min(fg_w, bg_w, fg_w + x_offset, bg_w - x_offset)
                h = min(fg_h, bg_h, fg_h + y_offset, bg_h - y_offset)

                if w < 1 or h < 1:
                    return

                # clip foreground and background images to the overlapping regions
                bg_x = max(0, x_offset)
                bg_y = max(0, y_offset)
                fg_x = max(0, x_offset * -1)
                fg_y = max(0, y_offset * -1)
                foreground = foreground[fg_y : fg_y + h, fg_x : fg_x + w]
                background_subsection = background[bg_y : bg_y + h, bg_x : bg_x + w]

                # separate alpha and color channels from the foreground image
                foreground_colors = foreground[:, :, :3]
                alpha_channel = foreground[:, :, 3] / 255  # 0-255 => 0.0-1.0

                # construct an alpha_mask that matches the image shape
                alpha_mask = np.dstack((alpha_channel, alpha_channel, alpha_channel))

                # combine the background with the overlay image weighted by alpha
                composite = (
                    background_subsection * (1 - alpha_mask)
                    + foreground_colors * alpha_mask
                )

                # overwrite the section of the background image that has been updated
                background[bg_y : bg_y + h, bg_x : bg_x + w] = composite
    return background


# Добавление пленки
def add_film(img, p=0.33):
    if random.uniform(0, 1) < p:
        film_file = random.choice(os.listdir("images/watermarks/films/"))
        film = cv2.imread(f"images/watermarks/films/{film_file}")
        # Аугментация отображение горизонтально, вертикально или вместе
        transform = A.Flip(always_apply=False, p=0.75)
        random.randrange(0, 100)
        film = transform(image=film)["image"]
        # высота и ширина пленки
        h_film, w_film = film.shape[:2]
        # высота и ширина паспорта
        h_img, w_img = img.shape[:2]
        # координаты центра
        center_x = int(w_img / 2)
        center_y = 550
        # расчет расстояний от центра
        top_y = center_y - int(h_film / 2)
        left_x = center_x - int(w_film / 2)
        bottom_y = top_y + h_film
        right_x = left_x + w_film
        # добавление пленки на паспорт
        roi = img[top_y:bottom_y, left_x:right_x]
        result = cv2.addWeighted(roi, 1, film, 0.2, 0)
        img[top_y:bottom_y, left_x:right_x] = result

        img = add_wm(img)
    return img


# Аугментация бланка паспорта
def augment_blank(image):
    transform = A.HueSaturationValue(
        hue_shift_limit=10, sat_shift_limit=20, val_shift_limit=20, p=0.9
    )
    return transform(image=image)["image"]


# Пайплайн аугментаций
def add_background(img, bboxes, p=0.5):
    if random.uniform(0, 1) < p:
        # Добавление фона
        background_file = random.choice(os.listdir("images/tables/"))
        background = cv2.imread(f"images/tables/{background_file}")
        background = cv2.cvtColor(background, cv2.COLOR_BGR2RGB)
        x_add = random.randint(50, 100)
        y_add = random.randint(50, 100)
        background[y_add : y_add + img.shape[0], x_add : x_add + img.shape[1]] = img
        img = background
        new_boxes = []
        for i in bboxes:
            x1, y1, x3, y3 = i
            new_boxes.append([x1 + x_add, y1 + y_add, x3, y3])
        bboxes = new_boxes
    return img, bboxes


def augment_passport(image, bboxes, p=0.9):
    class_labels = [
        "issued_line1",
        "issued_line2",
        "issued_line3",
        "issue_date",
        "issue_devision",
        "surname",
        "first_name",
        "middle_name",
        "sex",
        "birth_date",
        "birth_place1",
        "birth_place2",
        "birth_place3",
        "pass_no_top",
        "pass_no_bottom",
        "photo",
    ]
    transform = A.Compose(
        [
            A.OpticalDistortion(
                distort_limit=[-0.2, 0.2],
                shift_limit=0.05,
                interpolation=1,
                border_mode=3,
                value=None,
                mask_value=None,
                always_apply=False,
                p=0.5,
            ),
            A.Rotate(
                limit=5,
                interpolation=2,
                border_mode=3,
                value=None,
                mask_value=None,
                rotate_method="largest_box",
                crop_border=False,
                always_apply=False,
                p=0.5,
            ),
            A.RandomCropFromBorders(
                crop_left=0.1,
                crop_right=0.05,
                crop_top=0.1,
                crop_bottom=0.1,
                always_apply=False,
                p=0.5,
            ),
            A.RandomBrightnessContrast(
                brightness_limit=0.2,
                contrast_limit=0.2,
                brightness_by_max=True,
                always_apply=False,
                p=0.8,
            ),
            A.Downscale(
                scale_min=0.8,
                scale_max=0.99,
                interpolation=cv2.INTER_AREA,
                always_apply=False,
                p=0.3,
            ),
            A.GaussNoise(
                var_limit=(50.0, 200.0),
                mean=0,
                per_channel=True,
                always_apply=False,
                p=0.5,
            ),
            A.Blur(blur_limit=2, always_apply=False, p=0.2),
            A.RandomSunFlare(
                flare_roi=(0, 0, 1, 1),
                angle_lower=0,
                angle_upper=1,
                num_flare_circles_lower=1,
                num_flare_circles_upper=2,
                src_radius=75,
                src_color=(255, 255, 255),
                always_apply=False,
                p=0.5,
            ),
            A.OneOf(
                [A.ToGray(always_apply=False), A.ToSepia(always_apply=False)], p=0.1
            ),
        ],
        bbox_params=A.BboxParams(format="coco", label_fields=["class_labels"]),
        p=p,
    )

    # Применение трансформаций
    transformed = transform(image=image, bboxes=bboxes, class_labels=class_labels)
    img = transformed["image"]
    bboxes = transformed["bboxes"]
    return img, bboxes
