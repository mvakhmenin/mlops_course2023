from PIL import ImageFont, ImageDraw, Image, ImageOps
import numpy as np
import cv2
import random
from faker import (
    Faker,
)  # библиотека генерации фейковых данных https://faker.readthedocs.io/en/master/locales/ru_RU.html
import matplotlib.pyplot as plt
import jsonlines
import json
import os
import shutil
from tqdm import tqdm
from augmentations_defs import add_film, augment_passport, augment_blank, add_background
import click


@click.command()
@click.option(
    "--num_train", prompt="Enter number of images to generate", type=click.INT
)
def passport_generation(num_train):
    # ЗАПОЛНЯЕТСЯ ВРУЧНУЮ
    # Количество изображений в выборках
    # num_train = 10
    num_validation = 0
    num_test = 0

    # Создание папок train, validation, test
    dirs = ["datasets/train", "datasets/test", "datasets/validation"]
    for dir in dirs:
        if os.path.exists(dir):
            shutil.rmtree(dir)
        os.makedirs(dir)

    cat_list = {
        "Паспорт выдан": 1,
        "Дата выдачи": 2,
        "Код подразделения": 3,
        "Фамилия": 4,
        "Имя": 5,
        "Отчество": 6,
        "Пол": 7,
        "Дата рождения": 8,
        "Место рождения": 9,
        "Серия номер": 10,
    }

    ###
    # COCO annotation dicts
    coco_info = []
    coco_licenses = []
    coco_images = []
    coco_annotations = []
    coco_categories = [
        {"id": 0, "name": "issued_line1", "supercategory": ""},
        {"id": 1, "name": "issued_line2", "supercategory": ""},
        {"id": 2, "name": "issued_line3", "supercategory": ""},
        {"id": 3, "name": "issue_date", "supercategory": ""},
        {"id": 4, "name": "issue_devision", "supercategory": ""},
        {"id": 5, "name": "surname", "supercategory": ""},
        {"id": 6, "name": "first_name", "supercategory": ""},
        {"id": 7, "name": "middle_name", "supercategory": ""},
        {"id": 8, "name": "sex", "supercategory": ""},
        {"id": 9, "name": "birth_date", "supercategory": ""},
        {"id": 10, "name": "birth_place1", "supercategory": ""},
        {"id": 11, "name": "birth_place2", "supercategory": ""},
        {"id": 12, "name": "birth_place3", "supercategory": ""},
        {"id": 13, "name": "pass_no_top", "supercategory": ""},
        {"id": 14, "name": "pass_no_bottom", "supercategory": ""},
        {"id": 15, "name": "photo", "supercategory": ""},
    ]
    coco_segment_info = []
    annotation_id = 0

    for i in tqdm(range(num_train + num_validation + num_test), desc="Создание..."):
        # Определение типа выборки:
        if 0 <= i < num_train:
            split = "train"
        elif num_train <= i < num_train + num_validation:
            split = "validation"
        else:
            split = "test"

        # Открытие изображения и задание цвета текста
        img = Image.open("images/blank01.png")
        b, g, r, a = 0, 0, 0, 0
        draw = ImageDraw.Draw(img)
        fake = Faker(["ru_RU"])

        #####################
        ### Для разметки COCO
        image_id = i
        file_name = f"{i}.jpg"

        #####################
        ### Отступы для разметки COCO
        h_pad = int(round(img.size[0] * 0.01))
        v_pad = int(round(h_pad / 3))

        #####################

        # Путь к шрифту
        rand = random.randint(0, 2)
        fontpath = random.choice(os.listdir("fonts/text"))
        fontpath1 = random.choice(os.listdir("fonts/number"))
        font = ImageFont.truetype(
            f"fonts/text/{fontpath}", 17 + rand
        )  # путь к шрифту для текста и размер
        font1 = ImageFont.truetype(
            f"fonts/number/{fontpath1}", 18
        )  # путь к шрифту для серии номера паспорта и размер

        # ГЕНЕРАЦИЯ ТЕКСТОВЫХ ПОЛЕЙ
        # верхняя часть
        # кем выдан подразделение
        otdel1 = random.choice(
            [
                "ОТДЕЛОМ УФМС",
                "ОТДЕЛОМ ВНУТРЕННИХ ДЕЛ",
                "ОТДЕЛЕНИЕМ УФМС РОССИИ",
                "ПАСПОРТНО-ВИЗОВОЕ ОТДЕЛЕНИЕ",
                "ОТДЕЛЕНИЕМ МИЛИЦИИ",
            ]
        )
        otdel2 = fake.administrative_unit().upper()  # кем выдан область
        otdel3 = fake.street_name().upper()  # кем выдан улица
        date_given = fake.date_of_birth(None, 0, 98).strftime(
            "%d.%m.%Y"
        )  # дата выдачи паспорта
        code_given = (
            fake.plate_number() + "-" + fake.plate_number()
        )  # код подразделения

        # нижняя часть
        last_name = fake.last_name().upper()  # фамилия
        first_name = fake.first_name().upper()  # имя
        middle_name = fake.middle_name().upper()  # отчество
        gender = random.choice(
            ["МУЖ", "ЖЕН", "МУЖ.", "ЖЕН.", "М", "Ж", "М.", "Ж."]
        )  # пол М,Ж
        date_birth = fake.date_of_birth(None, 18, 115).strftime(
            "%d.%m.%Y"
        )  # дата рождения
        birth_place1 = fake.city().upper()  # село/деревня или город, если один
        birth_place2 = fake.city().upper()  # город
        birth_place3 = fake.administrative_unit().upper()  # область/республика/край

        # поле серии номера паспорта
        number = (
            fake.plate_number()[:2]
            + "    "
            + fake.plate_number()[:2]
            + "    "
            + fake.postcode()
        )  # серия, номер паспорта

        # СОЗДАНИЕ JSON
        jsondata = {}
        ground_truth = {}
        gt_parse = {}
        meta = {}
        valid_line = []
        roi = {}
        repeating_symbol = []
        dontcare = []

        bboxes = []

        # РАСПОЛОЖЕНИЕ ТЕКСТОВЫХ ПОЛЕЙ НА ПАСПОРТЕ
        # ВЕРХНЯЯ ЧАСТЬ
        # поле кем выдан
        len_otdel1 = draw.textlength(otdel1, font)
        len_otdel2 = draw.textlength(otdel2, font)
        len_otdel3 = draw.textlength(otdel3, font)
        rand = random.randint(0, 10)
        draw.text(
            (270 + rand - len_otdel1 / 2, 65 + rand),
            otdel1,
            font=font,
            fill=(b, g, r, a),
        )
        draw.text(
            (270 + rand - len_otdel2 / 2, 90 + rand),
            otdel2,
            font=font,
            fill=(b, g, r, a),
        )
        draw.text(
            (270 + rand - len_otdel3 / 2, 120 + rand),
            otdel3,
            font=font,
            fill=(b, g, r, a),
        )
        # получение координат текста https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html#PIL.ImageDraw.ImageDraw.textbbox
        quad1 = draw.textbbox(
            (270 + rand - len_otdel1 / 2, 65 + rand), otdel1, font=font
        )  # bounding box (left, top, right, bottom)
        x1, y1, x3, y3 = quad1

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=0,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": otdel1, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Паспорт выдан", otdel1])
        quad2 = draw.textbbox(
            (270 + rand - len_otdel2 / 2, 90 + rand), otdel2, font=font
        )  # bounding box (left, top, right, bottom)
        x1, y1, x3, y3 = quad2

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=1,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": otdel2, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Паспорт выдан", otdel2])
        quad3 = draw.textbbox(
            (270 + rand - len_otdel3 / 2, 120 + rand), otdel3, font=font
        )  # bounding box (left, top, right, bottom)
        x1, y1, x3, y3 = quad3

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=2,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": otdel3, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Паспорт выдан", otdel3])
        # создание элементов json 'gt_parse'
        gt_parse["Паспорт выдан"] = [
            dict(name=otdel1),
            dict(name=otdel2),
            dict(name=otdel3),
        ]

        # поле дата выдачи
        rand = random.randint(0, 5)
        draw.text((100 + rand, 155 + rand), date_given, font=font, fill=(b, g, r, a))
        # получение координат текста
        quad = draw.textbbox((100 + rand, 155 + rand), date_given, font=font)
        x1, y1, x3, y3 = quad

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=3,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": date_given, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Дата выдачи", date_given])
        # создание элементов json 'gt_parse'
        gt_parse["Дата выдачи"] = dict(name=date_given)

        # поле код подразделения
        rand = random.randint(0, 5)
        draw.text((300 + rand, 155 + rand), code_given, font=font, fill=(b, g, r, a))
        # получение координат текста
        quad = draw.textbbox((300 + rand, 155 + rand), code_given, font=font)
        x1, y1, x3, y3 = quad

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=4,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": code_given, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Код подразделения", code_given])
        quad = dict(
            x2=int(x2),
            y3=int(y3),
            x3=int(x3),
            y4=int(y4),
            x1=int(x1),
            y1=int(y1),
            x4=int(x4),
            y2=int(y2),
        )
        # создание элементов json 'gt_parse'
        gt_parse["Код подразделения"] = dict(name=code_given)

        # НИЖНЯЯ ЧАСТЬ
        # поле ФИО
        len_last_name = draw.textlength(last_name, font)
        len_first_name = draw.textlength(first_name, font)
        len_middle_name = draw.textlength(middle_name, font)
        rand = random.randint(0, 10)
        draw.text(
            (320 + rand - len_last_name / 2, 400 + rand),
            last_name,
            font=font,
            fill=(b, g, r, a),
        )
        draw.text(
            (320 + rand - len_first_name / 2, 455 + rand),
            first_name,
            font=font,
            fill=(b, g, r, a),
        )
        draw.text(
            (320 + rand - len_middle_name / 2, 485 + rand),
            middle_name,
            font=font,
            fill=(b, g, r, a),
        )
        # получение координат текста
        quad1 = draw.textbbox(
            (320 + rand - len_last_name / 2, 400 + rand), last_name, font=font
        )
        x1, y1, x3, y3 = quad1

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=5,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": last_name, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Фамилия", last_name])

        quad2 = draw.textbbox(
            (320 + rand - len_first_name / 2, 455 + rand), first_name, font=font
        )
        x1, y1, x3, y3 = quad2

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=6,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": first_name, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Имя", first_name])

        quad3 = draw.textbbox(
            (320 + rand - len_middle_name / 2, 485 + rand), middle_name, font=font
        )
        x1, y1, x3, y3 = quad3

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=7,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": middle_name, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Отчество", middle_name])

        # создание элементов json 'gt_parse'
        gt_parse["Фамилия"] = dict(name=last_name)
        gt_parse["Имя"] = dict(name=first_name)
        gt_parse["Отчество"] = dict(name=middle_name)

        # поле М,Ж
        rand = random.randint(0, 3)
        draw.text((195 + rand, 515 + rand), gender, font=font, fill=(b, g, r, a))
        # получение координат текста
        quad = draw.textbbox((195 + rand, 515 + rand), gender, font=font)
        x1, y1, x3, y3 = quad

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=8,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": gender, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Пол", gender])
        # создание элементов json 'gt_parse'
        gt_parse["Пол"] = dict(name=gender)

        # поле дата рождения
        rand = random.randint(0, 5)
        draw.text((300 + rand, 515 + rand), date_birth, font=font, fill=(b, g, r, a))
        # получение координат текста
        quad = draw.textbbox((300 + rand, 515 + rand), date_birth, font=font)
        x1, y1, x3, y3 = quad

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=9,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": date_birth, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Дата рождения", date_birth])
        # создание элементов json 'gt_parse'
        gt_parse["Дата рождения"] = dict(name=date_birth)

        # поле места рождения
        # ширина текста (считаем ширину, чтобы разместить по центру) https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html#PIL.ImageDraw.ImageDraw.textlength
        len_birth_place1 = draw.textlength(birth_place1, font)
        len_birth_place2 = draw.textlength(birth_place2, font)
        len_birth_place3 = draw.textlength(birth_place3, font)
        rand = random.randint(0, 5)  # генератор случайного сдвига
        draw.text(
            (320 + rand - len_birth_place1 / 2, 535 + rand),
            birth_place1,
            font=font,
            fill=(b, g, r, a),
        )
        draw.text(
            (320 + rand - len_birth_place2 / 2, 570 + rand),
            birth_place2,
            font=font,
            fill=(b, g, r, a),
        )
        draw.text(
            (320 + rand - len_birth_place3 / 2, 600 + rand),
            birth_place3,
            font=font,
            fill=(b, g, r, a),
        )
        # получение координат текста
        quad1 = draw.textbbox(
            (320 + rand - len_birth_place1 / 2, 535 + rand), birth_place1, font=font
        )
        x1, y1, x3, y3 = quad1

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=10,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": birth_place1, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Место рождения", birth_place1])

        quad2 = draw.textbbox(
            (320 + rand - len_birth_place2 / 2, 570 + rand), birth_place2, font=font
        )
        x1, y1, x3, y3 = quad2

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=11,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": birth_place2, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Место рождения", birth_place2])

        quad3 = draw.textbbox(
            (320 + rand - len_birth_place3 / 2, 600 + rand), birth_place3, font=font
        )
        x1, y1, x3, y3 = quad3

        #####################
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=12,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": birth_place3, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Место рождения", birth_place3])
        # создание элементов json 'gt_parse'
        gt_parse["Место рождения"] = [
            dict(name=birth_place1),
            dict(name=birth_place2),
            dict(name=birth_place3),
        ]

        # ПОЛЕ НОМЕРА ПАСПОРТА
        rand = random.randint(0, 5)
        # верхний номер
        txt = Image.new("L", (500, 50))
        d = ImageDraw.Draw(txt)
        d.text((0, 0), number, font=font1, fill=255)
        w = txt.rotate(-90, expand=True)
        img.paste(
            ImageOps.colorize(w, [255, 0, 0], [255, 0, 0]), (450 + rand, 100 + rand), w
        )
        # нижний номер
        d.text((0, 0), number, font=font1, fill=255)
        w = txt.rotate(-90, expand=True)
        img.paste(
            ImageOps.colorize(w, [255, 0, 0], [255, 0, 0]), (450 + rand, 450 + rand), w
        )
        # получение координат текста
        quad = draw.textbbox((450 + rand + 30, 100 + rand - 8), number, font=font)
        x1, y1, _, _ = quad
        x3, y3 = x1 + 18, y1 + 170

        ##################### Верхний номер паспорта
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=13,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[x1 - h_pad, y1 - v_pad, x3 - x1 + 2 * h_pad, y3 - y1 + 2 * v_pad],
                iscrowd=0,
                attributes={"text": number, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        ##################### Нижний номер паспорта
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=14,
                segmentation=[],
                area=(x3 - x1 + 2 * h_pad) * (y3 - y1 + 2 * v_pad),
                bbox=[
                    x1 - h_pad,
                    y1 - v_pad + 350,
                    x3 - x1 + 2 * h_pad,
                    y3 - y1 + 2 * v_pad,
                ],
                iscrowd=0,
                attributes={"text": number, "occluded": False, "rotation": 0.0},
            )
        )
        annotation_id += 1
        #####################

        #####################
        # Добавление фото
        #####################
        photo_file = random.choice(os.listdir("images/photo/"))
        photo_img = Image.open(f"images/photo/{photo_file}")
        photo_img = photo_img.resize((130, 165), Image.ANTIALIAS)
        x1, y1, width, hight = 35, 450, 130, 165
        img.paste(photo_img, (35, 450))

        ##################### Нижний номер паспорта
        ### COCO-разметка ###
        coco_annotations.append(
            dict(
                id=annotation_id,
                image_id=image_id,
                category_id=15,
                segmentation=[],
                area=(hight + 30 + 5) * (width + 30 + 5),
                bbox=[x1 - 30, y1 - 30, width + 30 + 5, hight + 30 + 5],
                iscrowd=0,
            )
        )
        annotation_id += 1
        #####################

        x2, y2, x4, y4 = x3, y1, x1, y3
        bboxes.append([x1, y1, x3, y3, "Серия номер", number])
        # создание элементов json 'gt_parse'
        gt_parse["Серия номер"] = dict(name=number)

        # добавление элементов json 'meta'
        meta["version"] = "0.1"
        meta["split"] = split
        meta["image_id"] = i
        image_size = dict(width=img.size[0], height=img.size[1])
        meta["image_size"] = image_size

        bbox_coco = []
        for coco_index in range(-16, 0):
            bbox_coco.append(coco_annotations[coco_index]["bbox"])

        # АУГМЕНТАЦИИ
        img = np.array(img)
        # Аугментация бланка паспорта
        img = augment_blank(img)
        # Добавление пленки и водяных знаков
        img = add_film(img)
        # Добавление фона
        try:
            img, bbox_coco = add_background(
                img,
                bbox_coco,
                # bboxes
            )
        except:
            pass
        # Аугментация паспорта с пленкой
        try:
            img, bbox_coco = augment_passport(
                img,
                bbox_coco,
            )
        except:
            pass

        for bbox_index, coco_index in zip(range(16), range(-16, 0)):
            coco_annotations[coco_index]["bbox"] = bbox_coco[bbox_index]

        #####################
        ### COCO image_id ###
        # Записываем информацию о файле в конце, так как нужно учесть изменение размеров при добавлении фона
        coco_images.append(
            dict(
                license=0,
                file_name=file_name,
                coco_url="",
                height=img.shape[0],
                width=img.shape[1],
                date_captured="",
                flickr_url="",
                id=image_id,
            )
        )
        #####################

        # Сохранение изображения
        img = img[:, :, [2, 1, 0]]  # перевод в RGB для cv2
        cv2.imwrite(
            f"datasets/{split}/{i}.jpg", img
        )  # сохранить, если исходный файл в openCV

    coco = dict(
        info=coco_info,
        licenses=coco_licenses,
        images=coco_images,
        annotations=coco_annotations,
        categories=coco_categories,
        segment_info=coco_segment_info,
    )
    with open("datasets/annotations.json", "w") as write_file:
        json.dump(coco, write_file)


if __name__ == "__main__":
    passport_generation()
