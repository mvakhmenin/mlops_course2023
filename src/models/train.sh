#!/bin/bash

python3 train_net.py \
    --dataset_name          cat17_50k_aug-layout \
    --json_annotation_train ../../data/cat17_50k_aug/annotations_train.json \
    --image_path_train      ../../data/cat17_50k_aug/img_train \
    --json_annotation_val   ../../data/cat17_50k_aug/annotations_val.json \
    --image_path_val        ../../data/cat17_50k_aug/img_val \
    --config-file           ../../models/configs/cat17_50k_aug/HJfaster_rcnn_R_50_FPN_3x.yaml \
    MODEL.WEIGHTS ../../models/configs/HJfaster_rcnn_R_50_FPN_3x.pth \
    OUTPUT_DIR  ../../models/outputs/cat17_50k_aug \
    MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE 256 \
    SOLVER.CHECKPOINT_PERIOD 30000 \
    SOLVER.MAX_ITER 100000 \
    SOLVER.IMS_PER_BATCH 2 \
    MODEL.DEVICE cuda:2 \
    DATALOADER.NUM_WORKERS 4